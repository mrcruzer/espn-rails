class MessagesController < ApplicationController

    before_action :authenticate_user!, except: [:index, :show]

    before_action :find_message, only: [:show, :edit, :update, :destroy]

    def index 
        @messages = Message.all
    end

    def show 
    end

    def new
        @message = Message.new 
    end

    def create
        @message = current_user.messages.new(message_params)

         respond_to do |format|
            if @message.save
              format.html { redirect_to @message, warning: 'Mensaje Creado Exitosamente.' }
              format.json { render :show, status: :created, location: @message }
            else
              format.html { render :new }
              format.json { render json: @message.errors, status: :unprocessable_entity }
            end
        end
    end

    def update
        respond_to do |format|
      if @message.update(message_params)
        format.html { redirect_to @message, warning: 'Mensaje actualizado exitosamente.' }
        format.json { render :show, status: :ok, location: @message }
      else
        format.html { render :edit }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
    end

    def edit 
    end

    def destroy
        @message.destroy
        respond_to do |format|
            format.html { redirect_to messages_url, warning: 'Mensaje Borrado Exitosamente.' }
            format.json { head :no_content }
        end
    end




    private
        def find_message
            @message = Message.find(params[:id])
        end

        def message_params
            params.require(:message).permit(:title, :description)
        end

end
